//https://www.codingame.com/ide/puzzle/ascii-art

const L = parseInt(readline());
const H = parseInt(readline());
const T = readline();
// A - Z = > 65 - 90
const firstUppercaseCode = 65;
// a - z = > 97 - 122
const firstLowercaseCode = 97;
const atCode = 64;


var letterIndex = (char) => char.charCodeAt() - useCode(char);

var useCode = (char) => {
  return(
    ( char.toUpperCase() === char && firstUppercaseCode )
      || ( char.toLowerCase() === char && firstLowercaseCode )
        || ( char === '@' && atCode )
  );
}

let ascii = ``;

for (let i = 0; i < H; i++) {
    const ROW = readline();
    T.split('').map((char) => {
      let index = letterIndex(char);
      index = index < 0 ? 26 : index;
      let letterPos = L * index;
      ascii += `${ROW.slice(letterPos, letterPos + L)}`;
    });
    ascii += `\n`;
}

console.log(ascii);
